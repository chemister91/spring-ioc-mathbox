import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MathBoxProxyImpl implements MathBox {
    private static List<Integer> intList = new LinkedList<>();

    private static final String ALREADYEXISTS = "Число уже есть в коллекции";
    private static final String NOTEXISTS = "Числа нет в коллекции";

    private MathBoxProxyImpl() {
    }

    private static MathBox getProxy() {
        return (MathBox) Proxy.newProxyInstance(
                CustomInvocationHandler.class.getClassLoader(),
                new Class[]{MathBox.class},
                new CustomInvocationHandler(new MathBoxProxyImpl()));
    }

    @Override
    public MathBox getInstance() {
        return getProxy();
    }

    @Override
    public void add(int value) throws IllegalAccessException {
        if (intList.contains(value)) {
            throw new IllegalAccessException(ALREADYEXISTS);
        } else {
            intList.add(value);
        }
    }

    @Override
    public void delete(int value) throws IllegalAccessException {
        if (intList.contains(value)) {
            intList.remove(Integer.valueOf(value));
        } else {
            throw new IllegalAccessException(NOTEXISTS);
        }
    }

    @Override
    public Integer getTotal() {
        return getIntStream().sum();
    }

    @Override
    public Double getAverage() {
        return getIntStream()
                .average()
                .getAsDouble();
    }

    @Override
    public Integer getMax() {
        return getIntStream()
                .max()
                .getAsInt();
    }

    @Override
    public Integer getMin() {
        return getIntStream()
                .min()
                .getAsInt();
    }

    @Override
    public Collection getMultiplied(int multiplicator) {
        intList = intList
                .stream()
                .map(i -> i * multiplicator)
                .collect(Collectors.toList());
        return intList;
    }

    @Override
    public Map<Integer, String> getMap() {
        return intList
                .stream()
                .collect(Collectors.toMap(i -> i, i -> String.valueOf(i)));
    }

    @Override
    public Collection getCollection() {
        return intList;
    }

    @Override
    public void clear() {
        intList.clear();
    }

    private IntStream getIntStream() {
        return intList
                .stream()
                .mapToInt(Integer::intValue);
    }
}
