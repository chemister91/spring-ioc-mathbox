import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class MathBoxEmptyImpl implements MathBox {
    private static final MathBoxEmptyImpl instance = new MathBoxEmptyImpl();

    @Override
    public MathBox getInstance() {
        return instance;
    }

    @Override
    public void add(int value) throws IllegalAccessException {

    }

    @Override
    public void delete(int value) throws IllegalAccessException {

    }

    @Override
    public Integer getTotal() {
        return 0;
    }

    @Override
    public Double getAverage() {
        return 0.0;
    }

    @Override
    public Integer getMax() {
        return 0;
    }

    @Override
    public Integer getMin() {
        return 0;
    }

    @Override
    public Collection getMultiplied(int multiplicator) {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Map<Integer, String> getMap() {
        return Collections.EMPTY_MAP;
    }

    @Override
    public Collection getCollection() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void clear() {

    }
}
