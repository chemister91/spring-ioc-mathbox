import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Logger;

public class CustomInvocationHandler implements InvocationHandler {
    private MathBox mathBox;
    private Method method;
    private final Logger logger = Logger.getLogger("InvocationHandler");

    public CustomInvocationHandler(MathBox mathBox) {
        this.mathBox = mathBox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        this.method = method;
        beforeExecution();
        Object object = method.invoke(mathBox, args);
        afterExecution();
        return object;
    }

    private void clearData() {
        if (method.isAnnotationPresent(ClearData.class)) {
            mathBox.clear();
        }
    }

    private void logData() {
        if (method.isAnnotationPresent(LogData.class)) {
            logger.info(mathBox.getCollection().toString());
        }
    }

    private void beforeExecution() {
        logData();
    }

    private void afterExecution() {
        clearData();
        logData();
    }

}
