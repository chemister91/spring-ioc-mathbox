import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        final Logger logger = Logger.getLogger(Main.class.getName());
        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
        MathBoxHandler handler = (MathBoxHandler) context.getBean("mathBoxHandler");
        MathBox mathBox = handler.getMathBox();
        try {
            mathBox.add(10);
            logger.info(mathBox.getTotal().toString());
        } catch (IllegalAccessException e) {
            logger.severe(e.getMessage());
        }
    }


}
